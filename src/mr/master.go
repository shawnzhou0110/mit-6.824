package mr

import (
	"fmt"
	"log"
	"sync"
)
import "net"
import "os"
import "net/rpc"
import "net/http"

import "time"

type taskState struct {
	state     int
	startTime time.Time
	fileName  string
	fileIdx   int
	reduceIdx int
	nFiles    int
	nReduce   int
	isNil     bool
}

func (t *taskState) setTime() {
	t.startTime = time.Now()
}

func (t *taskState) isOOT() bool {
	// crash test, may cause max normal delay = 10
	return time.Since(t.startTime) > time.Duration(time.Second*20)
}

//type taskInterface interface{
//	setTime()
//	isOOT() bool
//
//}

// thread safe queue
type queue struct {
	list  []taskState
	mutex sync.Mutex
}

func (q *queue) lock() {
	q.mutex.Lock()
}

func (q *queue) unlock() {
	q.mutex.Unlock()
}

func (q *queue) push(task taskState) {
	q.lock()
	q.list = append(q.list, task)
	q.unlock()
}

func (q *queue) pop() taskState {
	q.lock()
	length := len(q.list)
	if length == 0 {
		q.unlock()
		return taskState{isNil: true}
	}
	res := q.list[length-1]
	q.list = q.list[:length-1]
	q.unlock()
	return res
}

func (q *queue) getOOTList() []taskState {
	res := make([]taskState, 0)
	q.lock()
	for idx := 0; idx < len(q.list); {
		task := q.list[idx]
		if task.isOOT() {
			res = append(res, task)
			q.list = append(q.list[:idx], q.list[idx+1:]...)
		} else {
			idx++
		}
	}
	q.unlock()
	return res
}

func (q *queue) appendtoTail(oldTasks []taskState) {
	q.lock()
	q.list = append(q.list, oldTasks...)
	q.unlock()
}

func (q *queue) remove(fileIdx int, reduceIdx int) {
	q.lock()
	for idx := 0; idx < len(q.list); {
		task := q.list[idx]
		if task.fileIdx == fileIdx && task.reduceIdx == reduceIdx {
			q.list = append(q.list[:idx], q.list[idx+1:]...)
		} else {
			idx++
		}
	}
	q.unlock()
}

func (q *queue) size() int {
	return len(q.list)
}

type Master struct {
	// Your definitions here.
	isDone        bool
	fileNames     []string
	nReduce       int
	mapWaiting    queue
	mapRunning    queue
	reduceWaiting queue
	reduceRunning queue
}

func (m *Master) AskTask(args *ExampleArgs, reply *TaskInfo) error {
	if m.isDone {
		reply.State = taskEnd
		return nil
	}
	// fmt.Println("ask for a task, processing, master.go, line: 128")
	// m.mapWaiting.size() may cause thread-race
	// like if m.mapWaiting.size()>0
	task := m.mapWaiting.pop()
	if task.isNil == false {
		// handout map task
		// fmt.Println(task.fileName)
		reply.State = taskMap
		reply.FileName = task.fileName
		reply.FileIdx = task.fileIdx
		reply.ReduceIdx = task.reduceIdx
		reply.NFiles = task.nFiles
		reply.NReduce = task.nReduce
		m.mapRunning.push(task)
		log.Printf("[master]handout map task, taskIdx %v, taskName%s, master.go,line:142",task.fileIdx,task.fileName)
		return nil
	} else if m.mapRunning.size() > 0 {
		// just sleep, in case some worker break down
		reply.State = taskWait
		return nil
	}

	task = m.reduceWaiting.pop()
	if task.isNil == false {
		// handout reduce task
		reply.State = taskReduce
		reply.FileName = task.fileName
		reply.FileIdx = task.fileIdx
		reply.ReduceIdx = task.reduceIdx
		reply.NFiles = task.nFiles
		reply.NReduce = task.nReduce
		m.reduceRunning.push(task)
		log.Printf("[master]handout reduce task, reduceIdx %v, master.go,line:159",task.reduceIdx)
		return nil
	} else if m.reduceRunning.size() > 0 {
		// just sleep
		reply.State = taskWait
		return nil
	}

	reply.State = taskEnd
	return nil
}

func (m *Master) beginReduce() {
	log.Printf("[master]********** begin reduce ***********")
	log.Printf("[master]map Waiting len %v, map Running len %v",m.mapWaiting.size(),m.mapRunning.size())
	length := len(m.fileNames)
	for idx := 0; idx < m.nReduce; idx++ {
		task := taskState{
			state:     taskReduce,
			fileIdx:   0,
			reduceIdx: idx,
			nFiles:    length,
			nReduce:   m.nReduce,
		}
		task.setTime()
		m.reduceRunning.push(task)
	}
}

func (m *Master) TaskDone(args *TaskInfo, reply *TaskInfo) error {
	switch args.State {
	case taskMap:
		// remove certain map task
		// check if all map tasks are done
		// if so, then handout reduce
		log.Printf("[master]map task is done, fileidx %v",args.FileIdx)
		m.mapRunning.remove(args.FileIdx, args.ReduceIdx)
		if m.mapWaiting.size() == 0 && m.mapRunning.size() == 0 {
			m.beginReduce()
		}
	case taskReduce:
		// remove certain reduce task
		// check if all reduce tasks are done
		// if so, then set m.isDone true
		log.Printf("[master]reduce task is done, reduceidx %v",args.ReduceIdx)
		m.reduceRunning.remove(args.FileIdx, args.ReduceIdx)
		if m.reduceWaiting.size() == 0 && m.reduceRunning.size() == 0 {
			m.isDone = true
		}
	default:
		fmt.Println("something error when master is called taskDone, master.go line:196")
	}
	return nil
}

// Your code here -- RPC handlers for the worker to call.

//
// an example RPC handler.
//
// the RPC argument and reply types are defined in rpc.go.
//
func (m *Master) Example(args *ExampleArgs, reply *ExampleReply) error {
	reply.Y = args.X + 1
	return nil
}

//
// start a thread that listens for RPCs from worker.go
//
func (m *Master) server() {
	rpc.Register(m)
	rpc.HandleHTTP()
	//l, e := net.Listen("tcp", ":1234")
	sockname := masterSock()
	os.Remove(sockname)
	l, e := net.Listen("unix", sockname)
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)
}

//
// main/mrmaster.go calls Done() periodically to find out
// if the entire job has finished.
//
func (m *Master) Done() bool {
	// Your code here.
	return m.isDone
}

func (m *Master) observeTime() {
	for {
		time.Sleep(time.Second)
		if m.mapRunning.size() > 0 {
			OOTlist := m.mapRunning.getOOTList()
			m.mapWaiting.appendtoTail(OOTlist)
			OOTlist = make([]taskState, 0)
		} else if m.reduceRunning.size() > 0 {
			OOTlist := m.reduceRunning.getOOTList()
			m.reduceWaiting.appendtoTail(OOTlist)
			OOTlist = make([]taskState, 0)
		}
	}
}

//
// create a Master.
// main/mrmaster.go calls this function.
// nReduce is the number of reduce tasks to use.
//
func MakeMaster(files []string, nReduce int) *Master {
	m := Master{
		fileNames: files,
		nReduce:   nReduce,
	}

	// Your code here.
	length := len(files)
	// generate map tasks
	for idx, file := range files {
		task := taskState{
			state:     taskMap,
			fileName:  file,
			fileIdx:   idx,
			reduceIdx: 0,
			nFiles:    length,
			nReduce:   nReduce,
		}
		task.setTime()
		m.mapWaiting.push(task)
	}
	go m.observeTime()
	m.server()
	return &m
}
