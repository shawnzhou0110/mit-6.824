package mr

//
// RPC definitions.
//
// remember to capitalize all names.
//

import "os"
import "strconv"

//
// example to show how to declare the arguments
// and reply for an RPC.
//

type ExampleArgs struct {
	X int
}

type ExampleReply struct {
	Y int
}

// Add your RPC definitions here.
const (
	taskMap    = 1
	taskReduce = 2
	taskWait   = 3
	taskEnd    = 4
)

// TaskInfo for RPC format, this struct must start with Uppercase
type TaskInfo struct {
	State     int
	FileName  string
	FileIdx   int
	ReduceIdx int
	NFiles    int
	NReduce   int
}

// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the master.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func masterSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}
