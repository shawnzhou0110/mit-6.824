package mr

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sort"
	"strconv"
	"time"
)
import "log"
import "net/rpc"
import "hash/fnv"

//
// Map functions return a slice of KeyValue.
//
type KeyValue struct {
	Key   string
	Value string
}

//
// use ihash(key) % NReduce to choose the reduce
// task number for each KeyValue emitted by Map.
//
func ihash(key string) int {
	h := fnv.New32a()
	h.Write([]byte(key))
	return int(h.Sum32() & 0x7fffffff)
}

func askTask() TaskInfo {
	args := ExampleArgs{}
	reply := TaskInfo{}
	//fmt.Println("calling to work")
	call("Master.AskTask", &args, &reply)
	//fmt.Println(reply)
	return reply
}

func taskDone(info TaskInfo) {
	log.Printf("[worker]calling to done,state is %v, fileidx is %v",info.State,info.FileIdx)
	args := info
	reply := TaskInfo{}
	call("Master.TaskDone", &args, &reply)
	return
}

//
// main/mrworker.go calls this function.
//
func Worker(mapf func(string, string) []KeyValue,
	reducef func(string, []string) string) {

	// CallExample()
	// Your worker implementation here.
	for {
		task := askTask()
		switch task.State {
		case taskMap:
			performMap(task, mapf)
		case taskReduce:
			performReduce(task, reducef)
		case taskWait:
			time.Sleep(time.Second * 5)
		case taskEnd:
			return
		}
	}
	// uncomment to send the Example RPC to the master.
	// CallExample()

}

type ByKey []KeyValue

// for sorting by key.
func (a ByKey) Len() int           { return len(a) }
func (a ByKey) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByKey) Less(i, j int) bool { return a[i].Key < a[j].Key }

func performMap(task TaskInfo, mapf func(string, string) []KeyValue) {
	//	read filename as a dict
	//	open NReduce files as outfile
	//	ihash each kv pair and insert into correct outfile
	//	close all outfiles
	//	call taskDone() to change master's state

	// read in file and generate kva
	intermediate := []KeyValue{}

	file, err := os.Open(task.FileName)
	if err != nil {
		log.Fatalf("cannot open %v", file)
	}
	content, err := ioutil.ReadAll(file)
	if err != nil {
		log.Fatalf("cannot read %v", file)
	}
	file.Close()
	kva := mapf(task.FileName, string(content))
	intermediate = append(intermediate, kva...)

	// then hash and insert into different outfile

	// store file handler and JSON encoder
	outfiles := make([]*os.File, 0)
	encs := make([]*json.Encoder, 0)
	// open R files as output
	prefix := "mr-temp-" + strconv.Itoa(task.FileIdx) + "-"
	for i := 0; i < task.NReduce; i++ {
		//	outfile name `mr-temp-%v-%v`, fileIdx, i
		oname := prefix + strconv.Itoa(i)
		ofile, err := os.Create(oname)
		if err != nil {
			fmt.Println("wrong, worker.go, line: 119")
		}
		outfiles = append(outfiles, ofile)
		enc := json.NewEncoder(outfiles[i])
		encs = append(encs, enc)
	}
	// hash
	for _, pair := range intermediate {
		idx := ihash(pair.Key) % task.NReduce
		err := encs[idx].Encode(&pair)
		if err != nil {
			fmt.Println("wrong, worker.go, line:124")
		}
	}
	for _, outfile := range outfiles {
		outfile.Close()
	}
	taskDone(task)
}

func performReduce(task TaskInfo, reducef func(string, []string) string) {
	//	get reduceIdx
	//	read all files with reduceIdx
	//	merge
	//	sort
	//	generate one outfile, `mr-out-%v`, %v is reduceIdx
	//	close all files
	//	call taskDone() to change master's state

	prefix := "mr-temp-"
	suffix := "-" + strconv.Itoa(task.ReduceIdx)
	kva := []KeyValue{}

	// store files handler and decoder
	//infiles:=make([]*os.File,0)
	decs := make([]*json.Decoder, 0)
	for i := 0; i < task.NFiles; i++ {
		name := prefix + strconv.Itoa(i) + suffix
		file, err := os.Open(name)
		if err != nil {
			log.Fatalf("cannot open %v, worker.go, line:161", name)
		}
		decs = append(decs, json.NewDecoder(file))
		for {
			var kv KeyValue
			if err := decs[i].Decode(&kv); err != nil {
				break
			}
			kva = append(kva, kv)
		}
		file.Close()
	}

	sort.Sort(ByKey(kva))

	oname := "mr-out-" + strconv.Itoa(task.ReduceIdx)
	ofile, _ := os.Create(oname)
	i := 0
	for i < len(kva) {
		j := i + 1
		for j < len(kva) && kva[j].Key == kva[i].Key {
			j++
		}
		values := []string{}
		for k := i; k < j; k++ {
			values = append(values, kva[k].Value)
		}
		output := reducef(kva[i].Key, values)

		// this is the correct format for each line of Reduce output.
		fmt.Fprintf(ofile, "%v %v\n", kva[i].Key, output)

		i = j
	}

	ofile.Close()
	taskDone(task)
}

//
// example function to show how to make an RPC call to the master.
//
// the RPC argument and reply types are defined in rpc.go.
//
func CallExample() {

	// declare an argument structure.
	args := ExampleArgs{}

	// fill in the argument(s).
	args.X = 99

	// declare a reply structure.
	reply := ExampleReply{}

	// send the RPC request, wait for the reply.
	call("Master.Example", &args, &reply)

	// reply.Y should be 100.
	fmt.Printf("reply.Y %v\n", reply.Y)
}

//
// send an RPC request to the master, wait for the response.
// usually returns true.
// returns false if something goes wrong.
//
func call(rpcname string, args interface{}, reply interface{}) bool {
	// c, err := rpc.DialHTTP("tcp", "127.0.0.1"+":1234")
	sockname := masterSock()
	c, err := rpc.DialHTTP("unix", sockname)
	if err != nil {
		log.Fatal("dialing:", err)
	}
	defer c.Close()

	err = c.Call(rpcname, args, reply)
	if err == nil {
		return true
	}

	fmt.Println(err)
	return false
}
