package raft

type Status int

const (
	leader    Status = 1
	candidate Status = 2
	follower  Status = 3
)

const (
	NONE int = -1
)

func (rf *Raft) changeState(term int, state Status) {
	// rf.mu.Lock()
	rf.currentTerm = term
	rf.status = state
	// rf.mu.Unlock()
}

func (rf *Raft) becomeFollower(term int, votedFor int) {
	rf.mu.Lock()
	rf.currentTerm = term
	rf.status = follower
	rf.votedFor = votedFor
	rf.mu.Unlock()
}

//
func (rf *Raft) becomeCandidate() {
	rf.mu.Lock()
	rf.status = candidate
	rf.mu.Unlock()
}

//
func (rf *Raft) becomeLeader() {
	rf.mu.Lock()
	rf.status = leader
	rf.mu.Unlock()
}

func (rf *Raft) isLeader() bool {
	rf.mu.Lock()
	res := rf.status == leader
	rf.mu.Unlock()
	return res
}

// compareTermAndBecomeFollower 如果term大于rf的currentTerm，则设置currentTerm为term，且切换为跟随者
// If RPC request or response contains term T > currentTerm:
// set currentTerm = T, convert to follower (§5.1)
func (rf *Raft) compareTermAndBecomeFollower(term int) {
	if term > rf.currentTerm {
		switch rf.status {
		case candidate:
			DPrintf("[candidate-%v term-%v] become follower-%v term-%v", rf.me, rf.currentTerm, rf.me, term)
		case leader:
			DPrintf("[leader-%v term-%v] become follower-%v term-%v", rf.me, rf.currentTerm, rf.me, term)
		case follower:
			DPrintf("[follower-%v term-%v] become follower-%v term-%v", rf.me, rf.currentTerm, rf.me, term)
		}
		// DPrintf("[SWITCHSTATE: ->StateFollower]: raft[%v] receive other raft Term(%v) > self Term(%v).\n", rf.me, term, rf.currentTerm)
		rf.changeState(term, follower)
	}
}
