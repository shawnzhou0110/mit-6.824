package raft

//
// this is an outline of the API that raft must expose to
// the service (or tester). see comments below for
// each of these functions for more details.
//
// rf = Make(...)
//   create a new Raft server.
// rf.Start(command interface{}) (index, term, isleader)
//   start agreement on a new log entry
// rf.GetState() (term, isLeader)
//   ask a Raft for its current term, and whether it thinks it is leader
// ApplyMsg
//   each time a new entry is committed to the log, each Raft peer
//   should send an ApplyMsg to the service (or tester)
//   in the same server.
//

import (
	"math/rand"
	"sort"
	"sync"
	"time"
)
import "sync/atomic"
import "../labrpc"

// ApplyMsg
// as each Raft peer becomes aware that successive log entries are
// committed, the peer should send an ApplyMsg to the service (or
// tester) on the same server, via the applyCh passed to Make(). set
// CommandValid to true to indicate that the ApplyMsg contains a newly
// committed log entry.
//
// in Lab 3 you'll want to send other kinds of messages (e.g.,
// snapshots) on the applyCh; at that point you can add fields to
// ApplyMsg, but set CommandValid to false for these other uses.
//
type ApplyMsg struct {
	CommandValid bool
	Command      interface{}
	CommandIndex int
}

type Log struct {
	Command interface{} // 代表用于操作状态机的命令
	Term    int         // leader接收到命令的term
}

const (
	RPC_CALL_TIMEOUT time.Duration = time.Duration(1000) * time.Millisecond // 心跳RPC 投票RPC最大忍耐时间 400ms
	HEARTBEAT        time.Duration = time.Duration(100) * time.Millisecond  // 心跳周期100ms
)

type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]
	dead      int32               // set by Kill()

	// Your data here (2A, 2B, 2C).

	// 每个server都有的持久化数据
	currentTerm int   // 自己已知的最新term
	votedFor    int   // 自己投出去的候选人的id(如果没有候选人，则为-1)
	log         []Log // 本地的log(最主要的维护对象)

	status Status // 3种状态 1 leader 2 candidate 3 follower

	// 每个server都有的易失数据
	commitIndex int // log指针
	lastApplied int // 状态机的指针
	// 用于日志apply
	// commitIndex > lastApplied，那么server就需要将log applied 到状态机上

	// leader才会有的易失数据
	nextIndex  []int //
	matchIndex []int //

	// 只用于fellow转candidate状态，不用于 candidate 投票限制时间
	// fellow听到心跳，或者进行投票，则reset
	timer *time.Timer

	syncConds []*sync.Cond

	applyCh chan ApplyMsg // 每个raft用于apply的channel
	// applyCond *sync.Cond    // 每个raft用于控制apply的条件变量
	// applyCond chan int

	syncCount []int
}

func (rf *Raft) resetTimer() {
	rf.mu.Lock()
	rf.timer.Reset(time.Duration(400+rand.Intn(400)) * time.Millisecond)
	rf.mu.Unlock()
}

func (rf *Raft) getLastLogIndex() int {
	return len(rf.log) - 1
}

func (rf *Raft) getLastLogTerm() int {
	return rf.log[rf.getLastLogIndex()].Term
}

// Start
// the service using Raft (e.g. a k/v server) wants to start
// agreement on the next command to be appended to Raft's log. if this
// server isn't the leader, returns false. otherwise start the
// agreement and return immediately. there is no guarantee that this
// command will ever be committed to the Raft log, since the leader
// may fail or lose an election. even if the Raft instance has been killed,
// this function should return gracefully.
//
// the first return value is the index that the command will appear at
// if it's ever committed. the second return value is the current
// term. the third return value is true if this server believes it is
// the leader.
//
// Start
// 第一个返回值 新添加的cmd应该出现的idx
// 第二个返回值 当前term
// 第三个返回值 返回rf.isLeader
func (rf *Raft) Start(command interface{}) (int, int, bool) {

	// Your code here (2B).
	isLeader := false
	// 如果是leader就添加，否则不添加
	rf.mu.Lock()
	index := len(rf.log)
	curTerm := rf.currentTerm
	if rf.status == leader {
		rf.log = append(rf.log, Log{Term: rf.currentTerm, Command: command})
		// log.Printf("leader-%v len%v recv com-%v term-%v", rf.me, len(rf.log), command,rf.currentTerm)
		rf.nextIndex[rf.me] += 1
		rf.matchIndex[rf.me] += 1
		isLeader = true
		rf.persist()
	}
	rf.mu.Unlock()
	return index, curTerm, isLeader
}

// Kill
// the tester doesn't halt goroutines created by Raft after each test,
// but it does call the Kill() method. your code can use killed() to
// check whether Kill() has been called. the use of atomic avoids the
// need for a lock.
//
// the issue is that long-running goroutines use memory and may chew
// up CPU time, perhaps causing later tests to fail and generating
// confusing debug output. any goroutine with a long-running loop
// should call killed() to check whether it should stop.
//
// kill set
func (rf *Raft) Kill() {
	atomic.StoreInt32(&rf.dead, 1)
	// Your code here, if desired.
}

// killed用于检测raft实例的状态，如果已经是dead就返回true 否则返回false
// kill get
func (rf *Raft) killed() bool {
	z := atomic.LoadInt32(&rf.dead)
	return z == 1
}

func (rf *Raft) vote() {
	rf.currentTerm++
	rf.votedFor = rf.me
	rf.persist()
	args := RequestVoteArgs{
		Term:         rf.currentTerm,
		CandidateID:  rf.me,
		LastLogIndex: rf.getLastLogIndex(),
		LastLogTerm:  rf.getLastLogTerm(),
	}
	replyCh := make(chan RequestVoteReply, len(rf.peers))
	var wg sync.WaitGroup
	for i := range rf.peers {
		if i == rf.me {
			rf.resetTimer()
			continue
		}

		wg.Add(1)
		go func(server int) {
			defer wg.Done()
			reply := RequestVoteReply{}
			resCh := make(chan int)
			go func() {
				rf.sendRequestVote(server, &args, &reply)
				resCh <- 1
			}()
			select {
			case <-time.After(RPC_CALL_TIMEOUT):
				return
			case <-resCh:
				replyCh <- reply
			}
		}(i)
	}

	go func() {
		wg.Wait()
		close(replyCh)
	}()

	votes := 1
	major := len(rf.peers) / 2
	for reply := range replyCh {
		rf.mu.Lock()
		curTerm := rf.currentTerm
		rf.mu.Unlock()
		if reply.Term > curTerm {
			// 	->follower
			rf.becomeFollower(reply.Term, NONE)
			rf.persist()
			return
		}
		// if rf.status != candidate || reply.Term < rf.currentTerm{
		// 	return
		// }
		if reply.VoteGranted {
			votes++
		}
		if votes > major {
			// log.Printf("[candidate-%v term-%v] win %v votes, become leader", rf.me, rf.currentTerm, votes)
			rf.becomeLeader()
			// go rf.waitForElect()
			go rf.sync()
			go rf.heartBeat()
			// win
			return
		}
	}
	// 	这个select其实是多余的
	// select {
	// case <-rf.timer.C:
	// rf.resetTimer()
	// rf.becomeFollower(rf.currentTerm, NONE)
	// }
	rf.resetTimer()
	rf.becomeFollower(rf.currentTerm, NONE)
	rf.persist()
	return
}

func (rf *Raft) waitForElect() {
	for {
		select {
		case <-rf.timer.C: // election time out
			// log.Printf("raft-%v term-%v 开始进行选举",rf.me,rf.currentTerm)
			rf.becomeCandidate()
			rf.resetTimer()
			rf.vote() // 执行一遍，要么赢得选票，从而转化成leader并且创建leader线程，然后返回；要么选举失败，身份转化为follower，回来重新等待选举
		}
	}
}

func (rf *Raft) sync() {
	rf.matchIndex[rf.me] = len(rf.log) - 1
	for i := range rf.peers {
		if i == rf.me {
			rf.resetTimer()
			continue
		}
		// log.Printf("leader-%v 尝试创建sync-%v()", rf.me, i)
		go func(server int) {
			// 如果已经有了sync-server()正在运行，那么就退出
			rf.mu.Lock()
			if rf.syncCount[server] != 0 {
				// log.Printf("leader-%v sync-%v() 已经存在，准备退出", rf.me, server)
				rf.mu.Unlock()
				return
			}
			// 如果没有，那么就创建并++
			rf.syncCount[server]++
			rf.mu.Unlock()
			for {
				rf.syncConds[server].L.Lock()
				rf.syncConds[server].Wait()
				rf.syncConds[server].L.Unlock()

				rf.mu.Lock()
				if rf.status != leader {
					// log.Printf("raft-%v 身份从leader转换成%v，退出sync-%v 函数", rf.me, rf.status, server)
					rf.syncCount[server]--
					rf.mu.Unlock()
					return
				}
				// wait for trigger

				// log.Printf("leader-%v 尝试向raft-%v 发送消息，此时自己的term-%v", rf.me, server, rf.currentTerm)
				// 用lock的话，效率非常低，没法完成并发任务
				// append entries
				next := rf.nextIndex[server]
				args := AppendEntriesArgs{
					Term:         rf.currentTerm,
					LeaderID:     rf.me,
					Entries:      nil,
					LeaderCommit: rf.commitIndex,
					PrevLogIndex: next - 1,
					PrevLogTerm:  rf.log[next-1].Term,
				}
				if next < len(rf.log) {
					// 从idx=next开始 包括next 的日志，全部发送
					args.Entries = append(args.Entries, rf.log[next:]...)
				}
				rf.mu.Unlock()
				// log.Printf("leader-%v 向 raft-%v 发送日志，next is %v, leader的loglen-%v, 追加日志长度 %v", rf.me, server, next, len(rf.log), len(args.Entries))
				reply := AppendEntriesReply{}
				resCh := make(chan int)
				// 额外开启一个routine进行RPC，RPC完成之后 通过channel进行告知
				go func() {
					rf.sendAppendEntries(server, &args, &reply)
					resCh <- 1
				}()
				// 要么RPC超时，要么RPC完成
				select {
				case <-time.After(RPC_CALL_TIMEOUT):
					// RPC超时 直接进行下一次RPC
					// log.Printf("leader-%v append raft-%v len-%v timeout ", rf.me, server, len(args.Entries))
					continue
				case <-resCh:
				}
				// RPC完成，要么成功 要么失败
				if reply.Success == false {
					if reply.Term > rf.currentTerm {
						// log.Printf("leader-%v term-%v 由于raft-%v term-%v ，自己状态即将变成follwer", rf.me, rf.currentTerm, server, reply.Term)
						rf.becomeFollower(reply.Term, NONE)
						rf.resetTimer()
						rf.persist()
						rf.mu.Lock()
						rf.syncCount[server]--
						rf.mu.Unlock()
						return
					}
					// if rf.status != leader || reply.Term < rf.currentTerm {
					// 	return
					// }
					// follower 缺少太多log
					// log.Printf("leader-%v 说 raft-%v 缺少太多log，leader的log数量 %v， raft的log数量 %v", rf.me, server, len(rf.log), rf.matchIndex[i])
					// if rf.nextIndex[server] > 1 {
					// 	rf.nextIndex[server]--
					// }
					if reply.ConfIndex > 0 {
						firstConf := reply.ConfIndex
						if reply.ConfTerm != 0 {
							lastIdx := len(rf.log) - 1
							for i := 0; i <= lastIdx; i++ {
								if rf.log[i].Term != reply.ConfTerm {
									continue
								}
								for i <= lastIdx && rf.log[i].Term == reply.ConfTerm {
									i++ // the last conflict log's next index
								}
								firstConf = i
								break
							}
						}
						rf.nextIndex[server] = firstConf
					}
					// if reply.ConfTerm > 0 {
					// 	if reply.ConfTerm == 0 {
					// 		// 	代表日志缺失
					// 		rf.nextIndex[server] = max(1, reply.ConfIndex)
					// 	}else{
					// 		// 	代表日志term冲突
					// 		firstConf := 1
					// 		for i, log := range rf.log {
					// 			if log.Term == reply.ConfTerm{
					// 				firstConf = i
					// 				break
					// 			}
					// 		}
					// 		rf.nextIndex[server] = max(1, firstConf)
					// 	}
					// }
					continue
				}
				// log.Printf("[leader-%v term-%v loglen-%v, to raft-%v term-%v prevIdx-%v] append len-%v", rf.me, rf.currentTerm, len(rf.log), server, reply.Term, next, len(args.Entries))
				// 这里可以不用检查 reply.Success
				if reply.Success == true {
					// TODO
					rf.mu.Lock()
					rf.matchIndex[server] = args.PrevLogIndex + len(args.Entries) // conservative measurement of log agreement
					rf.nextIndex[server] = rf.matchIndex[server] + 1
					// rf.nextIndex[server] += len(args.Entries)
					// rf.matchIndex[server] = rf.nextIndex[server] - 1
					rf.updateCommitIndex()
					rf.mu.Unlock()
					// log.Printf("leader-%v update raft-%v next from %v to %v, should be %v", rf.me, server, next, rf.nextIndex[server], next+len(args.Entries))
				}
			}
		}(i)
	}
}

func (rf *Raft) updateCommitIndex() {
	n := len(rf.peers)
	match := make([]int, n)
	copy(match, rf.matchIndex)
	sort.Sort(sort.Reverse(sort.IntSlice(match))) // desc
	mid := match[n/2]
	// TODO
	if mid > rf.commitIndex && rf.log[mid].Term == rf.currentTerm {
		rf.commitIndex = mid
		rf.checkApply()
	}
}

func (rf *Raft) checkApply() {
	for rf.lastApplied < rf.commitIndex {
		rf.lastApplied++
		rf.applyCh <- ApplyMsg{
			CommandValid: true,
			Command:      rf.log[rf.lastApplied].Command,
			CommandIndex: rf.lastApplied,
		}
	}
}

// func (rf *Raft) leaderCommit() {
// 	for {
// 		if !rf.isLeader() {
// 			// log.Printf("raft-%v 身份从leader转换成%v，即将退出leaderCommit()函数", rf.me, rf.status)
// 			return
// 		}
// 		// time.Sleep(time.Millisecond*100)
// 		rf.mu.Lock()
// 		major := len(rf.peers) / 2
// 		n := len(rf.log)
// 		// log.Printf("leader-%v commitIndex-%v",rf.me,rf.commitIndex)
// 		for i := n - 1; i > rf.commitIndex; i-- { // looking for newest commit index from tail to head
// 			// in current term, if replicated on majority, commit it
// 			replicated := 0
// 			if rf.log[i].Term == rf.currentTerm {
// 				for server := range rf.peers {
// 					if rf.matchIndex[server] >= i {
// 						// log.Printf("leader-%v raft-%v matchIdx-%v i-%v",rf.me,server,rf.matchIndex[server],i)
// 						replicated += 1
// 					}
// 					// else{
// 					// 	log.Printf("leader-%v raft-%v matchIdx-%v i-%v NOT MATCH",rf.me,server,rf.matchIndex[server],i)
// 					//
// 					// }
// 				}
// 			}
//
// 			if replicated > major {
// 				// all (commitIndex, newest commitIndex] logs are committed
// 				// leader now apply them
// 				rf.applyCond.L.Lock()
// 				rf.applyCond.Broadcast()
// 				rf.applyCond.L.Unlock()
// 				// rf.applyCond <- 1
// 				rf.commitIndex = i
// 				// log.Printf("leader-%v 提交成功，commitIdx=%v", rf.me, rf.commitIndex)
// 				break
// 			}
// 		}
// 		rf.mu.Unlock()
// 	}
// }
//
// func (rf *Raft) waitApply() {
// 	for {
// 		// <-rf.applyCond
// 		rf.applyCond.L.Lock()
// 		rf.applyCond.Wait()
// 		rf.applyCond.L.Unlock()
// 		var logs []Log
// 		rf.mu.Lock()
// 		applied := rf.lastApplied
// 		committed := rf.commitIndex
// 		// (lastApplied, commitIndex]
// 		if applied < committed {
// 			for i := applied + 1; i <= committed; i++ {
// 				logs = append(logs, rf.log[i])
// 			}
// 			rf.lastApplied = committed
// 		}
// 		rf.mu.Unlock()
// 		// if applied < committed{
// 		// 	logs=append(logs,rf.log[applied:]...)
// 		// 	rf.lastApplied=committed
// 		// }
// 		// rf.mu.Unlock()
// 		for i, l := range logs {
// 			rf.applyCh <- ApplyMsg{
// 				CommandValid: true,
// 				Command:      l.Command,
// 				CommandIndex: applied + i + 1,
// 			}
// 			// log.Printf("raft-%v index-%v cmd-%v apply成功", rf.me, applied+1+i, l.Command)
// 		}
// 	}
// }

func (rf *Raft) heartBeat() {
	// time.Tick return 只读channel, 周期性触发
	ch := time.Tick(HEARTBEAT)
	for {
		if !rf.isLeader() {
			// log.Printf("raft-%v 身份从leader转换成%v，即将退出heartBeat()函数", rf.me, rf.status)
			return
		}
		for i := range rf.peers {
			if i == rf.me {
				rf.resetTimer()
				continue
			}
			rf.syncConds[i].L.Lock()
			rf.syncConds[i].Broadcast() // trigger
			rf.syncConds[i].L.Unlock()
		}
		// log.Printf("leader-%v heart beating...", rf.me)
		<-ch
	}
}

// Make
// the service or tester wants to create a Raft server. the ports
// of all the Raft servers (including this one) are in peers[]. this
// server's port is peers[me]. all the servers' peers[] arrays
// have the same order. persister is a place for this server to
// save its persistent state, and also initially holds the most
// recent saved state, if any. applyCh is a channel on which the
// tester or service expects Raft to send ApplyMsg messages.
// Make() must return quickly, so it should start goroutines
// for any long-running work.
//
func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me

	// Your initialization code here (2A, 2B, 2C).
	rf.currentTerm = 0
	rf.votedFor = -1
	rf.log = make([]Log, 1)
	rf.status = follower

	// volatile
	rf.commitIndex = 0
	rf.lastApplied = 0

	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())
	// leader
	rf.nextIndex = make([]int, len(rf.peers))
	rf.matchIndex = make([]int, len(rf.peers))
	for i := 0; i < len(rf.peers); i++ {
		rf.nextIndex[i] = len(rf.log)
		rf.matchIndex[i] = 0
	}

	// init 倒计时
	rf.timer = time.NewTimer(time.Duration(400+rand.Intn(400)) * time.Millisecond)
	rf.syncConds = make([]*sync.Cond, len(rf.peers))
	for i := range rf.peers {
		rf.syncConds[i] = sync.NewCond(&sync.Mutex{})
	}

	rf.applyCh = applyCh

	rf.syncCount = make([]int, len(rf.peers))
	for i := range rf.peers {
		rf.syncCount[i] = 0
	}

	go rf.waitForElect()

	return rf
}
