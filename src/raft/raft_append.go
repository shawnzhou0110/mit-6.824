package raft

// 心跳，添加log

type AppendEntriesArgs struct {
	Term         int
	LeaderID     int
	PrevLogIndex int
	PrevLogTerm  int
	Entries      []Log
	LeaderCommit int
}

type AppendEntriesReply struct {
	Term      int
	Success   bool
	ConfIndex int
	ConfTerm  int
}

// AppendEntries handler
func (rf *Raft) AppendEntries(args *AppendEntriesArgs, reply *AppendEntriesReply) {

	// reply
	reply.Success = false
	rf.mu.Lock()
	reply.Term = rf.currentTerm
	rf.mu.Unlock()
	// log.Printf("raft-%v 接收到leader-%v 的AppendEntriesRPC ",rf.me,args.LeaderID)

	// 不承认 old leader
	if args.Term < rf.currentTerm {
		// log.Printf("raft-%v status-%v term-%v不承认leader-%v term-%v", rf.me, rf.status, rf.currentTerm, args.LeaderID, args.Term)
		// 转告old leader最新的term
		return
	}

	// 说明新leader的term>自己的term，新leader已经产生，自己应该回退到follower状态
	if args.Term > rf.currentTerm {
		rf.currentTerm = args.Term
		// important! term更大，自己成为follower 并且清空 votedFor
		rf.becomeFollower(args.Term, NONE)
		rf.persist()
	}
	// 重置倒计时 无论 fellow 还是 candidate 都需要reset
	rf.resetTimer()
	// state转变，比如 leader 胜出之后，对其余所有发送心跳包，这个时候可能是 follower 可能是 candidate 收到心跳之后都会转化成 follower

	// 自己缺少的log太多了 or 日志的term不匹配 =>统称 一致性检查未通过
	prevIdx := args.PrevLogIndex
	if rf.getLastLogIndex() < prevIdx {
		// missing logs
		reply.ConfTerm = 0
		reply.ConfIndex = len(rf.log) - 1
		return
	}
	if rf.log[prevIdx].Term != args.PrevLogTerm {
		reply.ConfTerm = rf.log[prevIdx].Term
		for i, log := range rf.log {
			if log.Term == reply.ConfTerm {
				reply.ConfIndex = i
				break
			}
		}
		return
	}

	// 日志缺失，或者 日志term冲突
	// 应当首先通知leader，补齐log，然后自己再次接收到RPC的时候，先删除，再追加
	// last := len(rf.log) - 1
	committed := prevIdx
	// rf.mu.Lock()

	// log.Printf("raft-%v 接收到leader-%v 追加日志长度-%v",rf.me,args.LeaderID, len(args.Entries))
	if len(args.Entries) > 0 {
		rf.log = rf.log[:prevIdx+1]
		rf.log = append(rf.log, args.Entries...)
		committed = len(rf.log) - 1
	}

	// for i, entry := range args.Entries {
	// 	cur := prevIdx + i + 1
	// 	// 已有的进行覆盖
	// 	if cur <= last && rf.log[cur].Term != entry.Term {
	// 		rf.log[cur] = entry
	// 		committed = cur
	// 	}
	// 	// 新增的
	// 	if cur > last {
	// 		rf.log = append(rf.log, entry)
	// 		committed = len(rf.log) - 1
	// 	}
	// }
	// rf.log=rf.log[:committed+1]

	// leader已经进行commit了，raft才能进行commit
	if args.LeaderCommit > rf.commitIndex {
		// old := rf.commitIndex
		rf.commitIndex = min(committed, args.LeaderCommit)
		// log.Printf("raft-%v 开始进行commit, old-commit-idx=%v new-commit-idx=%v,commited=%v leadercommit=%v", rf.me, old, rf.commitIndex, committed, args.LeaderCommit)
		rf.checkApply()
	}
	// log.Printf("raft-%v 进行reply", rf.me)
	// log.Printf("raft-%v append %v %v",rf.me,len(args.Entries),len(rf.log))
	// reply
	rf.becomeFollower(args.Term, args.LeaderID)
	rf.persist()
	reply.Success = true
}

// RPC
func (rf *Raft) sendAppendEntries(server int, args *AppendEntriesArgs, reply *AppendEntriesReply) bool {
	ok := rf.peers[server].Call("Raft.AppendEntries", args, reply)
	return ok
}
