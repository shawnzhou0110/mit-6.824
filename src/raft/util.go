package raft

import "log"

// Debugging
const Debug = 0
const De2A = 0
const De2B = 0
const De2C = 0

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

func P2A(format string, a ...interface{}) (n int, err error) {
	if De2A > 0 {
		log.Printf(format, a...)
	}
	return
}

func P2B(format string, a ...interface{}) (n int, err error) {
	if De2B > 0 {
		log.Printf(format, a...)
	}
	return
}

func P2C(format string, a ...interface{}) (n int, err error) {
	if De2C > 0 {
		log.Printf(format, a...)
	}
	return
}

func min(a int, b int) int {
	if a < b {
		return a
	} else {
		return b
	}
}

func max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}
